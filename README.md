﻿## ***RidePal***

**RidePal Playlist Generator** enables your users to generate playlists for specific travel duration. RidePal calculates the travel duration time between the starting and destination locations and combines tracks chosen randomly in the specified genres, until the playing time roughly matches the travel time duration.

---
- Visibility without authentication
     - the application home page which contains top 6 playlists on the platform (they can be clicked to show the list of artists/tracks and more details)
     -   browse all public playlists, albums and artists
     - sort/filter/search functionality for playlists
     - the user registration form
     - the user login page
    
- When authenticated, user can:
   - create playlists
   - listen to playlists created by him or other users
   - see/delete/edit his own playlists
   - see/edit his personal information 

- The administrator page shows catalog of all playlists, albums, artists and users in the platform. Administrators can also edit/delete users/administrators and playlists.

- ***RidePal*** uses a playlist generating algorithm that:
   - allows the user to:
     -  choose whether he wants an artist to be repeated 
     - specify the approximate duration of each genre they include in the playlist by selecting percentage
     - specify if they want their playlist to be public or private
     - pick the highest-ranking tracks and generate a playlist with them
    - does not:
       - repeat tracks
       - generate the same playlist twice (repeating tracks/artists between playlists is allowed)
  - ensures the generated playlist's total duration and the calculated travel duration are within 5 minutes of difference
  



### ***Technologies***
- _Java 8_ - back-end development
- _HTML, CSS, JS_ - frond-end development
- _IntelliJ IDEA_ - IDE during the whole development process
-   _Spring MVC_ - application framework
-   _Bootstrap 4_ - for the front-end of the application making it responsive and providing ready to use stylesheets
-   _Thymeleaf_ - template engine 
-   _MariaDB_ - database
-   _Hibernate_ - ORM
-   _Spring Security_ - user management, roles and authentication
-   _Git_ - source control management

### ***Routes***
|Mapping | Description|
|--------|:------------|
|/ |Home page|
|/about |About page|
|/register|Registration page|
|/login|Login page|
|/playlists| Playlist page where all public playlists can be viewed, sorted and filtered|
|/playlists/{playlist_name}|A playlist's details page (for public playlists) where tracks can be played|
|/playlists/{username}/{playlist_id}|A playlist's detail page where users can view, edit and delete their public and private playlists|
|/generate|Playlist generator page where authenticated users can create their own playlists|
|/albums|Albums page|
|/album/{album_id}|An album's details page with all the songs in it|
|/artists| Artists page|
|/artists/{artist_id}|An artist's details page with all his albums|
|/users/{username}|A user's profile page|
|/users/{username}/update|Edit user's information page|
|/users|Admin page where administrators can see all existing users|

### ***Screenshots***

- Home page without authentication

![home_page](/images/home_page1.PNG)

![home_page](/images/home_page2.PNG)

- Home page with authentication

![home_page](/images/home_page4.png)

- Registration page

![register_page](/images/register_page.PNG)

- Login page

![login_page](/images/login_page.PNG)

- Playlist generator page

![generate_playlist](/images/generate_playlist.PNG)

- Playlist page

![playlists_page](/images/playlist_page.PNG)

- User's profile page

![profile_page](/images/user_details.PNG)

- Edit user's information
![update_page](/images/update_profile.PNG)

- Playlist's details page

![playlistDetalis_page](/images/playlist_details.png)


- Edit playlist's details 

![update_page](/images/update_playlist.png)

- Administrator page

![admin_page](/images/admin_panel1.png)
![admin_page](/images/admin_panel2.PNG)












 




















 





   


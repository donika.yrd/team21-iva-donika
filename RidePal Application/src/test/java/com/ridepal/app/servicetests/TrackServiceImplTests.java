package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.Track;
import com.ridepal.repositories.common.TracksRepository;
import com.ridepal.services.TracksServiceImpl;
import com.ridepal.services.common.GenresService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrackServiceImplTests {

    @Mock
    TracksRepository repository;

    @Mock
    GenresService genresService;

    @InjectMocks
    TracksServiceImpl mockService;

    @Test
    public void getAll_Should_CallRepository() {

        List<Track> expected = new ArrayList<>();
        expected.add(Factory.createTrack());

        when(repository.getAllTracks()).thenReturn(expected);

        List<Track> returned = mockService.getAllTracks();

        Mockito.verify(repository, times(1)).getAllTracks();
    }

    @Test
    public void getTrackByID_shouldCallRepository_whenTrackExist() {

        Track expected = Factory.createTrack();

        when(repository.getTrackById(anyInt()))
                .thenReturn(expected);

        Track returned = mockService.getTrackById(1);

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByID_shouldThrowException_whenTrackNotExist() {

        when(repository.getTrackById(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Track returned = mockService.getTrackById(anyInt());

        Mockito.verify(repository, times(1)).getTrackById(anyInt());
    }

    @Test
    public void getTrackByName_shouldCallRepository_whenTrackExist() {

        List<Track> expected = new ArrayList<>();
        expected.add(Factory.createTrack());

        when(repository.getTrackByName(anyString()))
                .thenReturn(expected);

        List<Track> returned = mockService.getTrackByName(anyString());

        Assert.assertSame(expected.get(0), returned.get(0));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByName_shouldThrowException_whenTrackNotExist() {

        when(repository.getTrackByName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        List<Track> returned = mockService.getTrackByName(anyString());

        Mockito.verify(repository, times(1)).getTrackByName(anyString());
    }

    @Test
    public void getTrackByGenre_shouldCallRepository_whenTrackExist() {

        List<Track> expected = new ArrayList<>();
        expected.add(Factory.createTrack());

        when(genresService.getGenreById(anyInt()))
                .thenReturn(Factory.createGenre());

        when(repository.getTracksByGenre(anyInt()))
                .thenReturn(expected);

        List<Track> returned = mockService.getTracksByGenre(anyInt());

        Mockito.verify(repository, times(1)).getTracksByGenre(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByGenre_shouldThrowException_whenTrackNotExist() {

        when(genresService.getGenreById(anyInt()))
                .thenReturn(Factory.createGenre());

        when(repository.getTracksByGenre(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        List<Track> returned = mockService.getTracksByGenre(anyInt());

        Mockito.verify(repository, times(1)).getTracksByGenre(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getTrackByGenre_shouldThrowException_whenGenreNotExist() {

        when(genresService.getGenreById(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        List<Track> returned = mockService.getTracksByGenre(anyInt());

        Mockito.verify(repository, times(1)).getTracksByGenre(anyInt());
    }

    @Test
    public void createGenre_shouldCallRepository() {
        Track expected = Factory.createTrack();

        when(repository.createTrack(anyString(), anyString(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyString()))
                .thenReturn(expected);

        Track returned = mockService.createTrack(anyString(), anyString(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyString());

        Mockito.verify(repository, times(1)).createTrack(anyString(), anyString(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyString());

    }


}

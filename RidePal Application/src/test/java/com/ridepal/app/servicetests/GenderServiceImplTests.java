package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.Gender;
import com.ridepal.models.Track;
import com.ridepal.repositories.GenderRepositoryImpl;
import com.ridepal.repositories.common.GendersRepository;
import com.ridepal.repositories.common.GenresRepository;
import com.ridepal.services.GenderServiceImpl;
import com.ridepal.services.common.GenderService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GenderServiceImplTests {

    @Mock
    GendersRepository repository;

    @InjectMocks
    GenderServiceImpl mockService;

    @Test
    public void getAll_Should_CallRepository() {

        List<Gender> expected = new ArrayList<>();
        expected.add(Factory.createGender());

        when(repository.getAllGenders()).thenReturn(expected);

        List<Gender> returned = mockService.getAllGender();

        Mockito.verify(repository, times(1)).getAllGenders();
    }

    @Test
    public void getGenderByID_shouldCallRepository_whenGenderExist() {

        Gender expected = Factory.createGender();

        when(repository.getGenderByID(anyInt()))
                .thenReturn(expected);

        Gender returned = mockService.getGenderByID(1);

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenderByID_shouldThrowException_whenGenderNotExist() {

        when(repository.getGenderByID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Gender returned = mockService.getGenderByID(anyInt());

        Mockito.verify(repository, times(1)).getGenderByID(anyInt());
    }

    @Test
    public void getTrackByName_shouldCallRepository_whenTrackExist() {

        Gender expected = Factory.createGender();

        when(repository.getGenderByName(anyString()))
                .thenReturn(expected);

        Gender returned = mockService.getGenderByName(anyString());

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenderByName_shouldThrowException_whenGenderNotExist() {

        when(repository.getGenderByName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        Gender returned = mockService.getGenderByName(anyString());

        Mockito.verify(repository, times(1)).getGenderByName(anyString());
    }

}

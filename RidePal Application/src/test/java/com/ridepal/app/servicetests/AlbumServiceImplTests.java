package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.Album;
import com.ridepal.models.Artist;
import com.ridepal.repositories.common.AlbumsRepository;
import com.ridepal.services.AlbumsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AlbumServiceImplTests {

    @Mock
    AlbumsRepository repository;

    @InjectMocks
    AlbumsServiceImpl mockService;

    @Test
    public void getAll_Should_CallRepository() {

        List<Album> expected = new ArrayList<>();
        expected.add(Factory.createAlbum());

        when(repository.getAllAlbums()).thenReturn(expected);

        List<Album> returned = mockService.getAllAlbums();

        Mockito.verify(repository, times(1)).getAllAlbums();
    }

    @Test
    public void getAlbumByID_shouldCallRepository_whenAlbumExist() {

        Album expected = Factory.createAlbum();

        when(repository.getAlbumByID(anyInt()))
                .thenReturn(expected);

        Album returned = mockService.getAlbumByID(1);

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumByID_shouldThrowException_whenAlbumNotExist() {

        when(repository.getAlbumByID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Album returned = mockService.getAlbumByID(anyInt());

        Mockito.verify(repository, times(1)).getAlbumByID(anyInt());
    }

    @Test
    public void getAlbumByDeezerID_shouldCallRepository_whenAlbumExist() {

        Album expected = Factory.createAlbum();

        when(repository.getAlbumByDeezerID(anyInt()))
                .thenReturn(expected);

        Album returned = mockService.getAlbumByDeezerID(1);

        Mockito.verify(repository, times(1)).getAlbumByDeezerID(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumByDeezerID_shouldThrowException_whenAlbumNotExist() {

        when(repository.getAlbumByDeezerID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Album returned = mockService.getAlbumByDeezerID(anyInt());

        Mockito.verify(repository, times(1)).getAlbumByDeezerID(anyInt());
    }

    @Test
    public void getAlbumByName_shouldCallRepository_whenAlbumExist() {

        List<Album> expected = new ArrayList<>();
        expected.add(Factory.createAlbum());

        when(repository.getAlbumByName(anyString()))
                .thenReturn(expected);

        List<Album> returned = mockService.getAlbumByName(anyString());

        Assert.assertSame(expected.get(0), returned.get(0));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAlbumByName_shouldThrowException_whenAlbumNotExist() {

        when(repository.getAlbumByName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        List<Album> returned = mockService.getAlbumByName(anyString());

        Mockito.verify(repository, times(1)).getAlbumByName(anyString());
    }

    @Test
    public void createAlbum_shouldCallRepository() {
        Album expected = Factory.createAlbum();

        when(repository.createAlbum(anyString(), anyInt(), anyString(), anyInt(), anyInt(), anyString()))
                .thenReturn(expected);

        Album returned = mockService.createAlbum(anyString(), anyInt(), anyString(), anyInt(), anyInt(), anyString());

        Mockito.verify(repository, times(1)).createAlbum(anyString(), anyInt(), anyString(), anyInt(), anyInt(), anyString());

    }
}

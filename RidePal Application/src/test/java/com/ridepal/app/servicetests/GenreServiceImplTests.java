package com.ridepal.app.servicetests;

import com.ridepal.app.Factory;
import com.ridepal.exceptions.DuplicateEntityException;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.models.Genre;
import com.ridepal.repositories.common.GenresRepository;
import com.ridepal.services.GenresServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GenreServiceImplTests {

    @Mock
    GenresRepository repository;

    @InjectMocks
    GenresServiceImpl mockService;

    @Test
    public void getAll_Should_CallRepository() {

        List<Genre> expected = new ArrayList<>();
        expected.add(Factory.createGenre());

        when(repository.getAllGenres())
                .thenReturn(expected);

        List<Genre> returned = mockService.getAllGenres();

        Mockito.verify(repository, times(1)).getAllGenres();
    }

    @Test
    public void getGenreByID_shouldCallRepository_whenGenreExist() {

        Genre expected = Factory.createGenre();

        when(repository.getGenreById(anyInt()))
                .thenReturn(expected);

        Genre returned = mockService.getGenreById(1);

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenreByID_shouldThrowException_whenGenreNotExist() {

        when(repository.getGenreById(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Genre returned = mockService.getGenreById(anyInt());

        Mockito.verify(repository, times(1)).getGenreById(anyInt());
    }

    @Test
    public void getGenreByName_shouldCallRepository_whenGenreExist() {

        List<Genre> expected = new ArrayList<>();
        expected.add(Factory.createGenre());

        when(repository.getGenreByName(anyString()))
                .thenReturn(expected);

        List<Genre> returned = mockService.getGenreByName(anyString());

        Assert.assertSame(expected.get(0), returned.get(0));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenreByName_shouldThrowException_whenGenreNotExist() {

        when(repository.getGenreByName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        List<Genre> returned = mockService.getGenreByName(anyString());

        Mockito.verify(repository, times(1)).getGenreByName(anyString());
    }

    @Test
    public void getGenreByExactName_shouldCallRepository_whenGenreExist() {
        Genre expected = Factory.createGenre();
        when(repository.getGenreByExactName(anyString()))
                .thenReturn(expected);

        Genre returned = mockService.getGenreByExactName(anyString());

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenreByExactName_shouldThrowException_whenGenreNotExist() {
        when(repository.getGenreByExactName(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        Genre returned = mockService.getGenreByExactName(anyString());

        Mockito.verify(repository, times(1)).getGenreByExactName(anyString());
    }

    @Test
    public void getGenreByDeezerID_shouldCallRepository_whenGenreExist() {

        Genre expected = Factory.createGenre();

        when(repository.getGenreByDeezerID(anyInt()))
                .thenReturn(expected);

        Genre returned = mockService.getGenreByDeezerID(anyInt());

        Assert.assertSame(expected, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getGenreByDeezerID_shouldThrowException_whenGenreNotExist() {

        when(repository.getGenreByDeezerID(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        Genre returned = mockService.getGenreByDeezerID(anyInt());

        Mockito.verify(repository, times(1))
                .getGenreByDeezerID(anyInt());
    }


    @Test
    public void createGenre_shouldCallRepository_whenNameNotRepeated() {

        Genre expected = Factory.createGenre();

        when(repository.getGenreByName(anyString()))
                .thenReturn(new ArrayList<>());

        when(repository.createGenre(anyString(), anyInt()))
                .thenReturn(expected);

        Genre returned = mockService.createGenre(expected.getName(), expected.getId());

        Mockito.verify(repository, times(1)).createGenre(anyString(), anyInt());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createGenre_shouldThrowException_whenNameRepeated() {

        when(repository.getGenreByName(anyString()))
                .thenThrow(new DuplicateEntityException(""));

        Genre returned = mockService.createGenre("test", 1);

        Mockito.verify(repository, times(1)).createGenre(anyString(), anyInt());
    }


    @Test
    public void getGenreByName_shouldCallRepository() {

        List<Genre> genres = new ArrayList<>();
        genres.add(Factory.createGenre());

        when(repository.getGenreByName(anyString()))
                .thenReturn(genres);

        List<Genre> returned = mockService.getGenreByName("test");

        Mockito.verify(repository, times(2))
                .getGenreByName(anyString());
    }

    @Test
    public void getGenreByNameForSearchMethod_shouldCallRepository() {
        Genre genre = Factory.createGenre();
        List<Genre> genres = new ArrayList<>();
        genres.add(genre);

        when(repository.getGenreByName(genre.getName())).thenReturn(genres);

        mockService.getGenreByNameForSearchMethod(genre.getName());

        Mockito.verify(repository, times(1)).getGenreByName(anyString());
    }
}

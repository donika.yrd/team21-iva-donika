package com.ridepal.controllers;

import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.validation.BindingResult;
import org.springframework.stereotype.Controller;
import com.ridepal.services.common.UsersService;
import com.ridepal.models.dtoes.UserRegDto;
import com.ridepal.models.dtoes.DtoMapper;
import org.springframework.ui.Model;
import javax.validation.Valid;

import static com.ridepal.utils.Constants.ALREADY_EXISTING_USERNAME_MESSAGE;
import static com.ridepal.utils.Helper.getErrorMessage;

@Controller
public class RegistrationController {
    private UserDetailsManager detailsManager;
    private UsersService usersService;
    private DtoMapper mapper;

    @Autowired
    public RegistrationController
            (@Qualifier("userDetailsManager") UserDetailsManager detailsManager,
             UsersService usersService,
             DtoMapper mapper) {
        this.detailsManager = detailsManager;
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping("/register")
    public String showRegistrationPage(Model model) {
        model.addAttribute("userRegDto", new UserRegDto());
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute UserRegDto userRegDto,
                           BindingResult bindingResult,
                           Model model) {
        String error = "";
        model.addAttribute("userRegDto", userRegDto);

        if (detailsManager.userExists(userRegDto.getUsername())) {
            error = String.format(
                    ALREADY_EXISTING_USERNAME_MESSAGE, userRegDto.getUsername());
            model.addAttribute("error", error);
            return "register";
        }
        if (bindingResult.hasErrors()) {
            error = getErrorMessage(bindingResult, error);
            model.addAttribute("error", error);
            return "register";
        }

        usersService.createUser(mapper.fromUseRegDto(userRegDto));
        return "redirect:/";
    }

    @GetMapping("/reg-confirm")
    public String showRegistrationConfirmPage(Model model) {
        return "registration-confirmation";
    }
}

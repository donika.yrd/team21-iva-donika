package com.ridepal.controllers.rest;

import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.bing_maps_connection.BingMapsConnector;
import com.ridepal.bing_maps_connection.RouteRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import java.math.BigInteger;

@RestController
@RequestMapping("/api/duration")
public class DurationRestController {
    private RouteRequest routeRequest;
    private BingMapsConnector bingMapConnector;

    @Autowired
    public DurationRestController() {
        this.routeRequest = new RouteRequest();
        this.bingMapConnector = new BingMapsConnector();
    }

    @GetMapping("/{wp1}/{wp2}")
    public BigInteger getDetails(@PathVariable String wp1,
                                 @PathVariable String wp2) {
        try {
            routeRequest.setStart_locality(wp1);
            routeRequest.setEnd_locality(wp2);

            return bingMapConnector.bingMapsConnection(routeRequest);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

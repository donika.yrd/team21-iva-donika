package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.TracksService;
import org.springframework.http.HttpStatus;
import com.ridepal.models.Track;

import java.util.List;

@RestController
@RequestMapping("/api/tracks")
public class TracksRestController {
    private TracksService tracksService;

    @Autowired
    public TracksRestController(TracksService tracksService) {
        this.tracksService = tracksService;
    }

    @GetMapping
    public List<Track> getAllTracks(){
        return tracksService.getAllTracks();
    }

    @GetMapping("/{name}")
    public List<Track> getTrackByName(@PathVariable String name){
        try{
            return tracksService.getTrackByName(name);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.ridepal.exceptions.DuplicateEntityException;
import com.ridepal.services.common.AlbumsService;
import com.ridepal.services.common.ArtistsService;
import com.ridepal.services.common.GenresService;
import com.ridepal.services.common.TracksService;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import com.ridepal.models.Artist;
import com.ridepal.models.Genre;
import com.ridepal.models.Track;
import com.ridepal.models.Album;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ridepal.utils.Helper.checkNumberOfAlbums;
import static com.ridepal.utils.Helper.checkNumberOfTracks;

@RestController
@RequestMapping("/api/deezer")
public class DeezerRestController {
    private RestTemplate restTemplate;
    private GenresService genresService;
    private ArtistsService artistsService;
    private AlbumsService albumsService;
    private TracksService tracksService;

    @Autowired
    public DeezerRestController(RestTemplate restTemplate,
                                GenresService genresService,
                                ArtistsService artistsService,
                                AlbumsService albumsService,
                                TracksService tracksService) {
        this.restTemplate = restTemplate;
        this.genresService = genresService;
        this.artistsService = artistsService;
        this.albumsService = albumsService;
        this.tracksService = tracksService;
    }

    @GetMapping(value = "/genres/{id}")
    public @ResponseBody
    Genre getGenreFromDeezer(@PathVariable int id) {
        String uri = "https://api.deezer.com/genre";
        Map<String, Integer> vars = new HashMap<>();
        vars.put("id", id);
                                                                     //Genre's IDs in Deezer:
                                                                     //111 - Techno/House
                                                                     //144 - Reggae
                                                                     //169 - Soul & Funk
        Genre genre = restTemplate.getForObject(uri + "/{id}", Genre.class, vars);

        try {
            genresService.createGenre(genre.getName(), id);
            genre.setDeezerId(id);
            return genre;
        }catch (DuplicateEntityException de){
            throw new ResponseStatusException(
                    HttpStatus.UNPROCESSABLE_ENTITY, de.getMessage());
        }
    }

    @GetMapping("/artists/{genreId}")
    public List<Artist> getArtistsFromDeezer(@PathVariable int genreId) {
        String uri = "https://api.deezer.com/genre/{genreID}";
        Map<String, Integer> vars = new HashMap<>();
        vars.put("genreID", genreId);

        ResponseEntity<String> response = restTemplate
                                          .getForEntity(uri + "/artists", String.class, vars);
        String artists = response.getBody();

        JSONObject object = new JSONObject(artists);
        JSONArray array = object.getJSONArray("data");

        for (int i = 0; i < array.length() - 1; i++) {
            String name = array.getJSONObject(i).getString("name");
            int id =  array.getJSONObject(i).getInt("id");
            String tracklist = array.getJSONObject(i).getString("tracklist");
            String picture = array.getJSONObject(i).getString("picture");

            artistsService.createArtist(name, id, tracklist, genreId, picture);
        }
        return artistsService.getAllArtists();
    }

    @GetMapping("/albums")
    public List<Album> getAlbumsFromDeezer() {
        Gson gson = new Gson();
        List<Integer> artistsIds = artistsService.getAllArtists()
                                                 .stream()
                                                 .map(Artist::getDeezerId)
                                                 .collect(Collectors.toList());
        for (int i = 0; i < artistsIds.size(); i++) {
            String uri = "https://api.deezer.com/artist/{artistId}";
            int currentId = artistsIds.get(i);
            Map<String, Integer> vars = new HashMap<>();
            vars.put("artistId", currentId);

            ResponseEntity<String> response = restTemplate
                                              .getForEntity(uri + "/albums", String.class, vars);
            String albums = response.getBody();

            JSONObject object = new JSONObject(albums);
            JSONArray array = object.getJSONArray("data");

//            int numberOfAlbums = checkNumberOfAlbums(array);
            for (int a = 0; a < array.length(); a++) {
                String title = array.getJSONObject(a).getString("title");
                int deezerId = array.getJSONObject(a).getInt("id");
                String tracklist = array.getJSONObject(a).getString("tracklist");
                String picture = array.getJSONObject(a).getString("cover");
                Artist artist = artistsService.getArtistByDeezerID(currentId);
                int genreId = artist.getGenreID();

                albumsService.createAlbum(title, deezerId, tracklist, genreId, currentId, picture);
            }
        }
        return albumsService.getAllAlbums();
    }

    @GetMapping("/tracks")
    public List<Track> getTracksFromDeezer() {
        List<Integer> albumsIds = albumsService.getAllAlbums()
                                               .stream()
                                               .map(Album::getDeezerId)
                                               .collect(Collectors.toList());
        for (int i = 0; i < albumsIds.size(); i++) {
            String uri = "https://api.deezer.com/album/{albumId}";
            int currentId = albumsIds.get(i);
            Map<String, Integer> vars = new HashMap<>();
            vars.put("albumId", currentId);

            ResponseEntity <String> response = restTemplate
                                              .getForEntity(uri + "/tracks", String.class, vars);
            String tracks = response.getBody();

            JSONObject object = new JSONObject(tracks);
            JSONArray array = object.getJSONArray("data");

//            int numberOfTracks = checkNumberOfTracks(array);
            for (int a = 0; a < array.length(); a++) {
                String title = array.getJSONObject(a).getString("title");
                String link = array.getJSONObject(a).getString("link");
                int duration = array.getJSONObject(a).getInt("duration");
                int deezerId = array.getJSONObject(a).getInt("id");
                Album album = albumsService.getAlbumByDeezerID(currentId);
                int artistId = album.getArtist().getDeezerId();
                int rank = array.getJSONObject(a).getInt("rank");
                String previewURL = array.getJSONObject(a).getString("preview");

                tracksService.createTrack(title, link, duration, deezerId, artistId, currentId, rank, previewURL);
            }
         }
        return tracksService.getAllTracks();
    }
}

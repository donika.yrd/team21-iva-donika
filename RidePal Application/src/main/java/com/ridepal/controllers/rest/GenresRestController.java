package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.ridepal.exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;
import com.ridepal.services.common.GenresService;
import org.springframework.http.HttpStatus;
import com.ridepal.models.Genre;

import java.util.List;

@RestController
@RequestMapping("/api/genres")
public class GenresRestController {
    private GenresService genresService;

    @Autowired
    public GenresRestController(GenresService service) {
        this.genresService = service;
    }

    @GetMapping
    public List<Genre> getAll() {
        return genresService.getAllGenres();
    }

    @GetMapping("/{name}")
    public List<Genre> getByName(@PathVariable String name) {
        try {
            return genresService.getGenreByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

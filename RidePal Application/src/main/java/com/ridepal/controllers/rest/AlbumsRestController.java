package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.AlbumsService;
import org.springframework.http.HttpStatus;
import com.ridepal.models.Album;

import java.util.List;

@RestController
@RequestMapping("/api/albums")
public class AlbumsRestController {
    private AlbumsService albumsService;

    @Autowired
    public AlbumsRestController(AlbumsService albumsService) {
        this.albumsService = albumsService;
    }

    @GetMapping
    public List<Album> findAllAlbums() {
        return albumsService.getAllAlbums();
    }

    @GetMapping("/{name}")
    public List<Album> findAlbumByName(@PathVariable String name) {
        try {
            return albumsService.getAlbumByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

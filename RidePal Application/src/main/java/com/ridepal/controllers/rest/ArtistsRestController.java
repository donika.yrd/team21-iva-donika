package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.ArtistsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import com.ridepal.models.Artist;

import java.util.List;

@RestController
@RequestMapping("/api/artists")
public class ArtistsRestController {
    private ArtistsService artistsService;

    @Autowired
    public ArtistsRestController(ArtistsService artistsService) {
        this.artistsService = artistsService;
    }

    @GetMapping
    public List<Artist> findAllArtists() {
        return artistsService.getAllArtists();
    }

    @GetMapping("/{name}")
    public List<Artist> findArtistByName(@PathVariable String name) {
        try {
            return artistsService.getArtistByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

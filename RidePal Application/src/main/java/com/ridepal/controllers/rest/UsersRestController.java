package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.ridepal.exceptions.DuplicateEntityException;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.PlaylistsService;
import org.springframework.web.bind.annotation.*;
import com.ridepal.services.common.UsersService;
import org.springframework.http.HttpStatus;
import com.ridepal.models.dtoes.UserRegDto;
import com.ridepal.models.dtoes.DtoMapper;

import com.ridepal.models.User;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UsersRestController {
    private UsersService usersService;
    private PlaylistsService playlistsService;
    private DtoMapper mapper;

    @Autowired
    public UsersRestController(UsersService usersService,
                               PlaylistsService playlistsService,
                               DtoMapper mapper) {
        this.usersService = usersService;
        this.playlistsService = playlistsService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<User> getAllUsers() {
        return usersService.getAllUsers();
    }

    @GetMapping("/{username}")
    public List<User> getUserByUsername(@PathVariable String username) {
        try {
            return usersService.getUsersWithExactUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/id/{id}")
    public User getUserByID(@PathVariable int id) {
        try {
            return usersService.getUserByID(id).get(0);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void createUser(@RequestBody @Valid UserRegDto userRegDto) {
        try {
            User newUser = mapper.fromUseRegDto(userRegDto);
            usersService.createUser(newUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException d) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, d.getMessage());
        }
    }

    @DeleteMapping("/{username}")
    public void deleteUser(@PathVariable String username) {
        try {
            usersService.deleteUser(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

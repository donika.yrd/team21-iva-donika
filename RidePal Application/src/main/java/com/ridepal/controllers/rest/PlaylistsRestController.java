package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.PlaylistsService;
import org.springframework.web.bind.annotation.*;
import com.ridepal.models.dtoes.PlaylistMinDto;
import org.springframework.http.HttpStatus;
import com.ridepal.models.dtoes.DtoMapper;
import com.ridepal.models.Playlist;
import org.omg.CORBA.BAD_PARAM;

import java.util.List;

@RestController
@RequestMapping("/api/playlists")
public class PlaylistsRestController {
    private PlaylistsService playlistsService;
    private DtoMapper mapper;

    @Autowired
    public PlaylistsRestController(DtoMapper mapper,
                                   PlaylistsService playlistsService) {
        this.playlistsService = playlistsService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<PlaylistMinDto> getAll(){
        return playlistsService.getAllPlaylists();
    }

    @GetMapping("/{name}")
    public List<Playlist> getPlaylistByName(@PathVariable String name){
        try{
            return playlistsService.getPlaylistByName(name);

        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/{id}")
//    public Playlist getPlaylistById(@PathVariable int id){
//        try{
//            return playlistsService.getPlaylistById(id);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

    @PutMapping("/{id}")
    public Playlist updatePlaylist(@PathVariable int id,
                                   @RequestBody PlaylistMinDto playlistMinDto){
        Playlist playlistUpdate = mapper.fromPlaylistMinDto(playlistMinDto);
        try {
            playlistsService.updatePlaylist(id, playlistUpdate);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return playlistUpdate;
    }

    @DeleteMapping("/{id}")
    public Playlist deletePlaylist(@PathVariable int id){
        try {
            return playlistsService.deletePlaylist(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/playlist/sort")
    public List<PlaylistMinDto> showSortedPlaylists(@RequestParam(defaultValue = "") String by){
        try{
            return playlistsService.sort(by);
        }catch (BAD_PARAM bp){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<PlaylistMinDto> showFilteredPlaylists(@RequestParam(defaultValue = "") String criteria,
                                                      @RequestParam (defaultValue = "") String ... value){
        try{
            return playlistsService.filter(criteria, value);
        }catch (BAD_PARAM bp){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }

    @GetMapping("/")
    public List<PlaylistMinDto> searchPlaylists(@RequestParam(defaultValue = "") String keyword){
        try{
            return playlistsService.searchPlaylists(keyword);
        }catch (BAD_PARAM bp){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }
}

package com.ridepal.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.GenderService;
import org.springframework.http.HttpStatus;
import com.ridepal.models.Gender;

import java.util.List;

@RestController
@RequestMapping("/api/genders")
public class GenderRestController {
    private GenderService genderService;

    @Autowired
    public GenderRestController(GenderService genderService) {
        this.genderService = genderService;
    }

    @GetMapping
    public List<Gender> getAllGenders() {
        return genderService.getAllGender();
    }

    @GetMapping("/{name}")
    public Gender getGender(@PathVariable String name) {
        try {
            return genderService.getGenderByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

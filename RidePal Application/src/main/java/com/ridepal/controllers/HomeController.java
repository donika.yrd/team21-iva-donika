package com.ridepal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import com.ridepal.services.common.PlaylistsService;
import org.springframework.stereotype.Controller;
import com.ridepal.models.dtoes.PlaylistMinDto;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    private PlaylistsService playlistsService;

    @Autowired
    public HomeController(PlaylistsService playlistsService) {
        this.playlistsService = playlistsService;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        List<PlaylistMinDto> getSortedDTOs = playlistsService.sort("rankdesc");
        List<PlaylistMinDto> sixMostRankedPlaylists = new ArrayList<>();
        if (getSortedDTOs.size() > 6){
            for (int i = 0; i < 6; i++) {
                sixMostRankedPlaylists.add(getSortedDTOs.get(i));
            }
        } else {
            sixMostRankedPlaylists.addAll(getSortedDTOs);
        }
        model.addAttribute("playlists", sixMostRankedPlaylists);
        return "index";
    }

    @GetMapping("/about")
    public String showAboutPage() {
        return "about_us";
    }

    @GetMapping("/faqs")
    public String showFaqs() {
        return "faqs";
    }

    @GetMapping("/privacy-policy")
    public String showPrivacyPolicyPage() {
        return "privacy_policy";
    }
}

package com.ridepal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import com.ridepal.services.common.ArtistsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.util.List;

@Controller
public class ArtistsController {
    private ArtistsService artistsService;

    @Autowired
    public ArtistsController(ArtistsService artistsService) {
        this.artistsService = artistsService;
    }

    @GetMapping("/artists")
    public String showAllArtists(Model model) {
        model.addAttribute("artists", artistsService.getAllArtists());
        return "artists_all_in_db";
    }

    @GetMapping("/artists/{id}")
    public String showArtistDetails(@PathVariable int id, Model model) {
        model.addAttribute("artist", artistsService.getArtistByID(id));
        return "artist_details";
    }
}

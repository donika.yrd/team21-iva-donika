package com.ridepal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.multipart.MultipartFile;
import com.ridepal.exceptions.EntityNotFoundException;
import static com.ridepal.utils.Helper.getLoggedUser;
import com.ridepal.services.common.PlaylistsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.ridepal.services.common.UsersService;
import com.ridepal.models.dtoes.PlaylistMinDto;
import org.springframework.http.HttpStatus;
import com.ridepal.models.dtoes.DtoMapper;
import org.springframework.ui.Model;
import com.ridepal.models.Playlist;
import com.ridepal.models.Genre;
import com.ridepal.models.Track;
import org.omg.CORBA.BAD_PARAM;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Controller
public class PlaylistsController {
    private PlaylistsService playlistsService;
    private UsersService usersService;
    private DtoMapper mapper;

    @Autowired
    public PlaylistsController(DtoMapper mapper,
                               UsersService usersService,
                               PlaylistsService playlistsService) {
        this.playlistsService = playlistsService;
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping("/playlists")
    public String showAllPlaylist(Model model) {
        String loggedUser = getLoggedUser();
        List<PlaylistMinDto> playlists = playlistsService.getAllPlaylists();
        model.addAttribute("playlists", playlists);
        model.addAttribute("loggedUser", loggedUser);

        return "playlists_all_in_db";
    }

    @GetMapping("/playlist/sort")
    public String showSortedPlaylists(@RequestParam(defaultValue = "") String by,
                                      Model model) {
        try {
            model.addAttribute("playlists", playlistsService.sort(by));
            return "playlists_all_in_db";
        } catch (BAD_PARAM bp) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }

    @GetMapping("/playlists/{username}/{playlistID}")
    public String showPrivatePlaylistDetailsToCreator(@PathVariable String username,
                                                      @PathVariable int playlistID,
                                                      Model model) {
        try {

            Playlist playlist = playlistsService.getPrivatePlaylistByName(username, playlistID);
            String numberOfTracks = playlistsService.getNumberOfTracks(playlist);
            String playtime = playlistsService.formatTotalPlaytime(playlist.getTotalPlaytime());
            String creator = usersService.getUserByID(playlist.getCreatorID()).get(0).getUsername();

            String loggedUser = getLoggedUser();
            Set<Track> tracks = playlist.getTracks();
            Set<Genre> genres = playlist.getGenresTags();
            boolean isAuthorized = usersService.isUserAuthorized(loggedUser, "ADMIN");

            if (!isAuthorized) {
                if (!username.equals(loggedUser)) {
                    return "access-denied";
                }
            }

            playlist.setNumberOfTracks(numberOfTracks);
            playlist.setPlaytime(playtime);

            model.addAttribute("numberOfTracks", numberOfTracks);
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("playlist", playlist);
            model.addAttribute("playtime", playtime);
            model.addAttribute("creator", creator);
            model.addAttribute("tracks", tracks);
            model.addAttribute("genres", genres);

            return "playlist_details";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/playlists/{title}")
    public String showPlaylist(@PathVariable String title,
                               Model model) {
        try {
            Playlist playlist = playlistsService.getPlaylistByName(title).get(0);
            String numberOfTracks = playlistsService.getNumberOfTracks(playlist);
            String playtime = playlistsService.formatTotalPlaytime(playlist.getTotalPlaytime());
            String creator = usersService.getUserByID(playlist.getCreatorID()).get(0).getUsername();

            String loggedUser = getLoggedUser();

            Set<Genre> genres = playlist.getGenresTags();
            Set<Track> tracks = playlist.getTracks();

            playlist.setNumberOfTracks(numberOfTracks);
            playlist.setPlaytime(playtime);

            model.addAttribute("numberOfTracks", numberOfTracks);
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("playlist", playlist);
            model.addAttribute("playtime", playtime);
            model.addAttribute("creator", creator);
            model.addAttribute("tracks", tracks);
            model.addAttribute("genres", genres);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "playlist_details";
    }

    @GetMapping("playlists/{title}/update")
    public String updatePlaylist(@PathVariable String title,
                                 PlaylistMinDto playlistMinDto,
                                 Model model) {
        String loggedUser = getLoggedUser();
        boolean isAuthorized = usersService.isUserAuthorized(loggedUser, "ADMIN");

        try {
            Playlist playlist = playlistsService.getPrivatePlaylistByName(title);
            String creator = playlist.getCreatorName();
            if (isAuthorized || creator.equals(loggedUser)) {
                model.addAttribute("playlistUpdateDto", playlistMinDto);
                model.addAttribute("playlist", playlist);
            } else {
                return "redirect:/access-denied";
            }

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "playlist_details";
    }

    @PostMapping("playlists/{title}/update")
    public String updatePlaylist(Model model,
                                 @PathVariable String title,
                                 @RequestParam(value = "file") MultipartFile file,
                                 @Valid @ModelAttribute("playlistUpdateDto") PlaylistMinDto playlistMinDto) {
        String loggedUser = getLoggedUser();
        int userId = usersService.getUsersWithExactUsername(loggedUser).get(0).getID();
        boolean isAuthorized = usersService.isUserAuthorized(loggedUser, "ADMIN");

        try {
            Playlist playlist = playlistsService.getPrivatePlaylistByName(title);
            int creatorId = playlist.getCreatorID();
            if (isAuthorized || creatorId == userId) {
                Playlist playlistToUpdate = playlistsService.getPrivatePlaylistByName(title);
                Playlist update = mapper.fromPlaylistMinDto(playlistMinDto);
                playlistsService.updatePlaylist(playlistToUpdate.getId(), update, file);
                model.addAttribute("playlistUpdateDto", playlistMinDto);
                model.addAttribute("playlist", playlist);
            } else {
                return "redirect:/access-denied";
            }
//
//            Playlist playlistToUpdate = playlistsService.getPlaylistByName(title).get(0);
//            Playlist update = mapper.fromPlaylistMinDto(playlistMinDto);
//            playlistsService.updatePlaylist(playlistToUpdate.getId(), update, file);


        } catch (EntityNotFoundException | IOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "redirect:/playlists";
    }

    @PostMapping("playlists/{title}/delete")
    public String deletePlaylist(@PathVariable String title) {
        String loggedUser = getLoggedUser();
        int userId = usersService.getUsersWithExactUsername(loggedUser).get(0).getID();
        boolean isAuthorized = usersService.isUserAuthorized(loggedUser, "ADMIN");

        try {
            Playlist playlist = playlistsService.getPrivatePlaylistByName(title);
            int creatorId = playlist.getCreatorID();
            if (isAuthorized || creatorId == userId) {
                Playlist playlistToDelete = playlistsService.getPrivatePlaylistByName(title);
                playlistsService.deletePlaylist(playlistToDelete.getId());
                return "redirect:/playlists";
            } else {
                return "redirect:/access-denied";
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/playlists/")
    public String searchPlaylists(@RequestParam(defaultValue = "") String keyword,
                                  Model model) {
        try {
            List<PlaylistMinDto> playlists = playlistsService.searchPlaylists(keyword);
            model.addAttribute("playlists", playlists);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "playlists_all_in_db";
    }
}
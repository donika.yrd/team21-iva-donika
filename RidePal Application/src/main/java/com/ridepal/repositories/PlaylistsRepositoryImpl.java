package com.ridepal.repositories;

import com.ridepal.models.Genre;
import com.ridepal.models.Playlist;
import com.ridepal.models.Track;
import com.ridepal.models.User;
import com.ridepal.repositories.common.PlaylistsRepository;
import com.ridepal.services.common.GenresService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.stereotype.Repository;
import com.ridepal.services.common.UsersService;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.Session;
import java.io.IOException;
import java.util.*;

@Repository
public class PlaylistsRepositoryImpl implements PlaylistsRepository {
    private SessionFactory sessionFactory;
    private GenresService genresService;
    private UsersService usersService;

    @Autowired
    public PlaylistsRepositoryImpl(SessionFactory sessionFactory,
                                   GenresService genresService,
                                   UsersService usersService) {
        this.sessionFactory = sessionFactory;
        this.genresService = genresService;
        this.usersService = usersService;
    }

    @Override
    public List<Playlist> getAllPlaylists() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true", Playlist.class).list();
    }

    @Override
    public Playlist getPlaylistById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query<Playlist> query = session.createQuery(
                "from Playlist where enabled = true and id = :id", Playlist.class);
        query.setParameter("id", id);
        return query.list().get(0);
    }

    @Override
    public List<Playlist> getPlaylistByName(String title) {
        Session session = sessionFactory.getCurrentSession();
        Query<Playlist> query = session.createQuery(
                "from Playlist where enabled = true and isPublic = true and title = :title", Playlist.class);
        query.setParameter("title", title);
        return query.list();
    }

    public Playlist getPrivatePlaylistByName(String username, int playlistID) {
        Session session = sessionFactory.getCurrentSession();

        User user = usersService.getUsersWithExactUsername(username).get(0);
        Query<Playlist> query = session.createQuery
                ("from Playlist where enabled=true and id=:playlistID and creatorID = :creatorID", Playlist.class);
        query.setParameter("playlistID", playlistID);
        query.setParameter("creatorID", user.getID());
        return query.list().get(0);
    }

    @Override
    public Playlist getPrivatePlaylistByName(String title) {
        Session session = sessionFactory.getCurrentSession();

        Query<Playlist> query = session.createQuery(
                "from Playlist where enabled = true and title = :title", Playlist.class);
        query.setParameter("title", title);
        return query.list().get(0);
    }

    @Override
    public List<Playlist> getPlaylistByCreator(int userID) {
        Session session = sessionFactory.getCurrentSession();
        Query<Playlist> query = session.createQuery
                ("from Playlist where enabled = true and creatorID = :userID", Playlist.class);
        query.setParameter("userID", userID);
        return query.list();
    }

    @Override
    public Playlist updatePlaylist(int id, Playlist playlist) {
        Session session = sessionFactory.getCurrentSession();
        Playlist playlistToUpdate = getPlaylistById(id);

        if (playlist.getTitle() != null) {
            playlistToUpdate.setTitle(playlist.getTitle());
        }

        if (!playlist.getGenresTags().isEmpty()) {
            playlistToUpdate.setGenresTags(playlist.getGenresTags());
        }

        if (playlist.getTotalPlaytime() != 0) {
            playlistToUpdate.setTotalPlaytime(playlist.getTotalPlaytime());
        }

        playlistToUpdate.setPublic(playlist.isPublic());

        session.beginTransaction();
        session.update(playlistToUpdate);
        session.getTransaction().commit();
        return playlistToUpdate;
    }

    @Override
    public Playlist updatePlaylist(int id, Playlist playlist, MultipartFile file) throws IOException {
        Session session = sessionFactory.getCurrentSession();
        Playlist playlistToUpdate = getPlaylistById(id);

        if (playlist.getTitle() != null) {
            playlistToUpdate.setTitle(playlist.getTitle());
        }

        if (!playlist.getGenresTags().isEmpty()) {
            playlistToUpdate.setGenresTags(playlist.getGenresTags());
        }

        if (playlist.getTotalPlaytime() != 0) {
            playlistToUpdate.setTotalPlaytime(playlist.getTotalPlaytime());
        }

        if (!file.isEmpty()) {
            playlistToUpdate.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
        }

        if (playlist.getCreatorName() != null) {
            playlistToUpdate.setCreatorName(playlist.getCreatorName());
        }

        playlistToUpdate.setPublic(playlist.isPublic());

        session.beginTransaction();
        session.update(playlistToUpdate);
        session.getTransaction().commit();
        return playlistToUpdate;
    }

    @Override
    public Playlist deletePlaylist(int id) {
        Session session = sessionFactory.getCurrentSession();
        Playlist playlistToDelete = getPlaylistById(id);
        playlistToDelete.setEnabled(false);

        session.beginTransaction();
        session.update(playlistToDelete);
        session.getTransaction().commit();

        return playlistToDelete;
    }

    @Override
    public Playlist createPlaylist(String title,
                                   int totalPlaytime,
                                   int creatorID,
                                   Set<Track> tracks,
                                   Set<Genre> genres,
                                   boolean isPublic) {
        Session session = sessionFactory.getCurrentSession();

        Playlist newPlaylist = new Playlist();
        newPlaylist.setTitle(title);
        newPlaylist.setTotalPlaytime(totalPlaytime);
        newPlaylist.setCreatorID(creatorID);
        newPlaylist.setTracks(tracks);
        newPlaylist.setGenresTags(genres);
        newPlaylist.setPublic(isPublic);

        session.beginTransaction();
        session.save(newPlaylist);
        session.getTransaction().commit();

        return newPlaylist;
    }

    public List<Playlist> filterByCreator(String creator) {
        Session session = sessionFactory.getCurrentSession();
        List<User> users = usersService.getUserForSearchMethod(creator);
        List<Playlist> result = new ArrayList<>();

        if (users.size() != 0) {
            for (User user : users) {
                int id = user.getID();
                Query<Playlist> query = session.createQuery(
                        "from Playlist  where enabled = true and isPublic= true and creatorID = :id", Playlist.class);
                query.setParameter("id", id);
                result.addAll(query.list());
            }
        }
        return result;
    }

    public List<Playlist> filterByDuration(int startSec, int endSec) {
        Session session = sessionFactory.getCurrentSession();
        Query<Playlist> query = session.createQuery
                ("from Playlist where enabled = true and isPublic = true and totalPlaytime between :startSec and :endSec", Playlist.class);
        query.setParameter("startSec", startSec);
        query.setParameter("endSec", endSec);
        return query.list();
    }

    public List<Playlist> filterByGenre(String genre) {
        Session session = sessionFactory.getCurrentSession();
        List<Genre> genres = genresService.getGenreByNameForSearchMethod(genre);
        List<Integer> playlists = new ArrayList<>();
        List<Playlist> result = new ArrayList<>();

        if (genres.size() != 0) {

            for (Genre g : genres) {
                int id = g.getId();
                Query query = session.createSQLQuery
                        ("select PlaylistID from ride_pal.playlists_genres " +
                                    "JOIN ride_pal.playlists ON playlists_genres.PlaylistID = playlists.ID " +
                                    "where genreID = :id AND ride_pal.playlists.Enabled = true");
                query.setParameter("id", id);
                playlists.addAll(query.list());
            }

            for (Integer i : playlists) {
                if (getPlaylistById(i).isPublic()) {
                    result.add(getPlaylistById(i));
                }
            }
        }
        return result;
    }

    private List<Playlist> filterByTitle(String title) {
        Session session = sessionFactory.getCurrentSession();

        Query query1 = session.createQuery(
                "from Playlist where enabled = true and isPublic = true and title like :title");
        query1.setParameter("title", "%" + title + "%");
        return query1.list();
    }

    public List<Playlist> searchPlaylists(String keyword) {
        Set<Playlist> allResults = new HashSet<>();
        allResults.addAll(filterByTitle(keyword));
        allResults.addAll(filterByCreator(keyword));
        allResults.addAll(filterByGenre(keyword));

        return new ArrayList<>(allResults);
    }

    public List<Playlist> sortByDurationAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true order by totalPlaytime asc", Playlist.class).list();
    }

    public List<Playlist> sortByDurationDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true order by totalPlaytime desc", Playlist.class).list();
    }

    public List<Playlist> sortByNameAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true order by title asc", Playlist.class).list();
    }

    public List<Playlist> sortByNameDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true order by title desc", Playlist.class).list();
    }

    @Override
    public List<Playlist> sortByRankAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true order by rank asc", Playlist.class).list();
    }

    @Override
    public List<Playlist> sortByRankDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from Playlist where enabled = true and isPublic = true order by rank desc", Playlist.class).list();
    }
}

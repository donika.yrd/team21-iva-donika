package com.ridepal.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.UsersRepository;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import com.ridepal.utils.Helper;
import com.ridepal.models.User;
import org.hibernate.Session;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Repository
public class UsersRepositoryImpl implements UsersRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from User where enabled = true", User.class).list();
    }

    @Override
    public List<User> getUsersWithCommonUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery(
                "from User where enabled = true and username like :username", User.class);
        query.setParameter("username", "%" + username + "%");
        return query.list();
    }

    @Override
    public List<User> getUserByID(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery(
                "from User where enabled = true and ID=:id", User.class);
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public void createUser(User userToCreate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userToCreate);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteUser(String username) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User user = getUserWithExactUsername(username).get(0);
        user.setEnabled(false);
        session.update(user);

        session.getTransaction().commit();
    }

    @Override
    public void updateUser(String username, User update) {
        User userToUpdate = getUserWithExactUsername(username).get(0);

        if (update.getFirstName() != null) {
            userToUpdate.setFirstName(update.getFirstName());
        }
        if (update.getLastName() != null) {
            userToUpdate.setLastName(update.getLastName());
        }
        if (update.getPassword() != null) {
            userToUpdate.setPassword(update.getPassword());
        }
        if (update.getEmail() != null) {
            userToUpdate.setEmail(update.getEmail());
        }
        if (update.getGender() != null) {
            userToUpdate.setGender(update.getGender());
        }
        if (!update.getPlaylists().isEmpty()) {
            userToUpdate.setPlaylists(update.getPlaylists());
        }

        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(userToUpdate);
        session.getTransaction().commit();
    }

    @Override
    public void updateUser(String username, User update, MultipartFile file) throws IOException {
        User userToUpdate = getUserWithExactUsername(username).get(0);
        Helper.updateParameters(userToUpdate, update);
        if (!file.isEmpty()) {
            userToUpdate.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
        }

        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(userToUpdate);
        session.getTransaction().commit();
    }

    @Override
    public List<User> getUserByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery(
                "from User where enabled = true and email = :email", User.class);
        query.setParameter("email", email);
        return query.list();
    }

    @Override
    public List<User> getUserWithExactUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery(
                "from User where enabled = true and username = :username", User.class);
        query.setParameter("username", username);
        return query.list();
    }

    public boolean isUserAuthorized(String username, String authority) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery(
                "select username from authorities where username=:username and authority=:authority");
        query.setParameter("username", username);
        query.setParameter("authority", authority);
        if (query.list().size() == 0) {
            return false;
        }
        return true;
    }
}

package com.ridepal.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.ArtistsRepository;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;
import com.ridepal.models.Artist;
import org.hibernate.query.Query;
import org.hibernate.Session;
import java.util.List;

import static com.ridepal.utils.Helper.setArtist;

@Repository
public class ArtistsRepositoryImpl implements ArtistsRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public ArtistsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Artist> getAllArtists() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Artist", Artist.class)
                .setMaxResults(50)
                .list();
    }

    @Override
    public List<Artist> getArtistByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Artist> query = session.createQuery(
                "from Artist where name like :name", Artist.class);
        query.setParameter("name", "%" + name + "%");

        return query.list();
    }

    @Override
    public Artist getArtistByID(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Artist.class, id);
    }

    @Override
    public List<Artist> getArtistByDeezerID(int deezerId){
        Session session = sessionFactory.getCurrentSession();
        Query<Artist> query = session.createQuery(
                "from Artist where deezerId = :deezerId", Artist.class);
        query.setParameter("deezerId", deezerId);

        return query.list();
    }

    @Override
    public Artist createArtist(String name, int deezerId, String tracklist, int genreId, String picture) {
        Session session = sessionFactory.getCurrentSession();
        Artist newArtist = setArtist(name, deezerId, tracklist, genreId, picture);

        session.beginTransaction();
        session.save(newArtist);
        session.getTransaction().commit();

        return newArtist;
    }
}

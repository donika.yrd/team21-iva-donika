package com.ridepal.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.TracksRepository;
import com.ridepal.services.common.ArtistsService;
import com.ridepal.services.common.AlbumsService;
import static com.ridepal.utils.Helper.setTrack;
import org.springframework.stereotype.Repository;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import com.ridepal.models.Artist;
import com.ridepal.models.Album;
import com.ridepal.models.Track;
import org.hibernate.Session;
import java.util.List;

@Repository
public class TracksRepositoryImpl implements TracksRepository {
    private SessionFactory sessionFactory;
    private ArtistsService artistsService;
    private AlbumsService albumsService;

    @Autowired
    public TracksRepositoryImpl(SessionFactory sessionFactory,
                                ArtistsService artistsService,
                                AlbumsService albumsService) {
        this.sessionFactory = sessionFactory;
        this.artistsService = artistsService;
        this.albumsService = albumsService;
    }

    @Override
    public List<Track> getAllTracks() {
        Session session = sessionFactory.getCurrentSession();
        Query<Track> query = session.createQuery(
                "from Track", Track.class);
        return query.list();
    }

    @Override
    public Track getTrackById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Track.class, id);
    }

    @Override
    public List<Track> getTrackByName(String title) {
        Session session = sessionFactory.getCurrentSession();
        Query<Track> query = session.createQuery(
                "from Track where title like :title", Track.class);
        query.setParameter("title", "%" + title + "%");
        return query.list();
    }

    @Override
    public List<Track> getTracksByGenre(int genreID) {
        Session session = sessionFactory.getCurrentSession();
        Query<Track> query = session.createQuery(
                "from Track where album.genreID=:genreID", Track.class);
        query.setParameter("genreID", genreID);
        return query.list();
    }

    @Override
    public Track createTrack(String title, String link,
                             int duration, int deezerId,
                             int artistId, int albumId,
                             int rank, String previewURL) {
        Session session = sessionFactory.getCurrentSession();

        Track newTrack = setTrack(title, link, duration, deezerId, rank, previewURL);
        Album album = albumsService.getAlbumByDeezerID(albumId);
        newTrack.setAlbum(album);
        Artist artist = artistsService.getArtistByDeezerID(artistId);
        newTrack.setArtist(artist);

        session.beginTransaction();
        session.save(newTrack);
        session.getTransaction().commit();

        return newTrack;
    }
}

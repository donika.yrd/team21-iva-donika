package com.ridepal.repositories.common;

import com.ridepal.models.Album;

import java.util.List;

public interface AlbumsRepository {
    List<Album> getAllAlbums();

    List<Album> getAlbumByName(String name);

    Album getAlbumByID(int id);

    Album getAlbumByDeezerID(int deezerId);

    Album createAlbum(String title, int deezerId, String tracklist, int genreId, int artistId, String picture);
}

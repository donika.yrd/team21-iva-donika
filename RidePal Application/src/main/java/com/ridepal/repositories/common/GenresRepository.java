package com.ridepal.repositories.common;

import com.ridepal.models.Genre;
import java.util.List;

public interface GenresRepository {
    List<Genre> getAllGenres();

    List<Genre> getGenreByName(String name);

    Genre getGenreById(int id);

    Genre createGenre(String name, int deezerId);

    Genre getGenreByDeezerID(int deezerID);

    Genre getGenreByExactName(String name);
}

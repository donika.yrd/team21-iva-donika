package com.ridepal.repositories.common;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ridepal.models.Gender;

import java.util.List;

public interface GendersRepository {
    List<Gender> getAllGenders();

    Gender getGenderByName(String name);

    Gender getGenderByID(int id);
}

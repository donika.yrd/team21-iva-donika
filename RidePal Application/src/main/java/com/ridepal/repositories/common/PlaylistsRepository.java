package com.ridepal.repositories.common;

import org.springframework.web.multipart.MultipartFile;
import com.ridepal.models.dtoes.PlaylistMinDto;
import com.ridepal.models.*;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface PlaylistsRepository {
    List<Playlist> getAllPlaylists();

    Playlist getPlaylistById(int id);

    List<Playlist> getPlaylistByName(String title);

    List<Playlist> getPlaylistByCreator(int userID);

    //used by rest controller
    Playlist updatePlaylist(int id, Playlist playlist);

    //used by mvc controller
    Playlist updatePlaylist(int id, Playlist playlist, MultipartFile file) throws IOException;

    Playlist deletePlaylist(int id);

    Playlist createPlaylist(String title, int totalPlaytime, int creatorID, Set<Track> tracks, Set<Genre> genres, boolean isPublic);

    List<Playlist> filterByCreator(String creator);

    List<Playlist> filterByDuration(int starSec, int endSec);

    List<Playlist> filterByGenre(String genre);

    List<Playlist> sortByNameAsc();

    List<Playlist> sortByNameDesc();

    List<Playlist> sortByDurationAsc();

    List<Playlist> sortByDurationDesc();

    List<Playlist> sortByRankAsc();

    List<Playlist> sortByRankDesc();

    List<Playlist> searchPlaylists(String keyword);

    Playlist getPrivatePlaylistByName(String username, int playlistID);

    Playlist getPrivatePlaylistByName(String playlistName);
}

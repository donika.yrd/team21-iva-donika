package com.ridepal.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.GenresRepository;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import com.ridepal.models.Genre;
import org.hibernate.Session;
import java.util.List;

@Repository
public class GenresRepositoryImpl implements GenresRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public GenresRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Genre> getAllGenres() {
        Session session = sessionFactory.getCurrentSession();
        Query<Genre> query = session.createQuery("from Genre", Genre.class);

        return query.list();
    }

    @Override
    public Genre getGenreById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Genre.class, id);
    }

    @Override
    public List<Genre> getGenreByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Genre> query = session.createQuery
                ("from Genre where name like :name", Genre.class);
        query.setParameter("name", "%" + name + "%");

        return query.list();
    }

    @Override
    public Genre getGenreByExactName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Genre> query = session.createQuery
                ("from Genre where name=:name", Genre.class);
        query.setParameter("name", name);
        return query.list().get(0);
    }

    public Genre getGenreByDeezerID(int deezerID){
        Session session = sessionFactory.getCurrentSession();
        Query<Genre> query = session.createQuery
                ("from Genre where deezerId = :deezerID", Genre.class);
        query.setParameter("deezerID", deezerID);

        return query.list().get(0);
    }

    @Override
    public Genre createGenre(String name, int deezerId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Genre newGenre = new Genre(name);
        newGenre.setDeezerId(deezerId);
        session.save(newGenre);
        session.getTransaction().commit();

        return newGenre;
    }
}

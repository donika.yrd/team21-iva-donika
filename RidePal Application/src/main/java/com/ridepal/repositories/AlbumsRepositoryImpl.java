package com.ridepal.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.AlbumsRepository;
import com.ridepal.services.common.ArtistsService;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;
import com.ridepal.models.Artist;
import org.hibernate.query.Query;
import com.ridepal.models.Album;
import org.hibernate.Session;
import java.util.List;

import static com.ridepal.utils.Helper.setAlbum;

@Repository
public class AlbumsRepositoryImpl implements AlbumsRepository {
    private SessionFactory sessionFactory;
    private ArtistsService artistsService;

    @Autowired
    public AlbumsRepositoryImpl(SessionFactory sessionFactory,
                                ArtistsService artistsService) {
        this.sessionFactory = sessionFactory;
        this.artistsService = artistsService;
    }

    @Override
    public List<Album> getAllAlbums() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Album", Album.class)
                .setMaxResults(50)
                .list();
    }

    @Override
    public List<Album> getAlbumByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Album> query = session.createQuery(
                "from Album where name like :name", Album.class);
        query.setParameter("name", "%" + name + "%");
        return query.list();
    }

    @Override
    public Album getAlbumByID(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Album.class, id);
    }

    @Override
    public Album getAlbumByDeezerID(int deezerId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Album> query = session.createQuery(
                "from Album where deezerId = :deezerId", Album.class);
        query.setParameter("deezerId", deezerId);

        return query.list().get(0);
    }

    @Override
    public Album createAlbum(String title, int deezerId, String tracklist, int genreId, int artistId, String picture) {
        Session session = sessionFactory.getCurrentSession();

        Album newAlbum = setAlbum(title, deezerId, tracklist, genreId, picture);
        Artist artist = artistsService.getArtistByDeezerID(artistId);
        newAlbum.setArtist(artist);

        session.beginTransaction();
        session.save(newAlbum);
        session.getTransaction().commit();

        return newAlbum;
    }
}

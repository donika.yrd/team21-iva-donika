package com.ridepal.services.common;

import com.ridepal.models.Album;
import java.util.List;

public interface AlbumsService {
    List<Album> getAllAlbums();

    List<Album> getAlbumByName(String name);

    Album getAlbumByID(int id);

    Album getAlbumByDeezerID(int deezerId);

    Album createAlbum(String title, int deezerId, String tracklist, int genreId, int artistId, String picture);
}

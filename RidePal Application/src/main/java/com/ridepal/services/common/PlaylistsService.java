package com.ridepal.services.common;

import org.springframework.web.multipart.MultipartFile;
import com.ridepal.models.dtoes.PlaylistMinDto;
import com.ridepal.models.*;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface PlaylistsService {
    List<PlaylistMinDto> getAllPlaylists();

    Playlist getPlaylistById(int id);

    List<Playlist> getPlaylistByName(String title);

    List<Playlist> getPlaylistsByCreator(int userID);

    //used by rest controller
    Playlist updatePlaylist(int id, Playlist playlist);

    //used by mvc controller
    Playlist updatePlaylist(int id, Playlist playlist, MultipartFile file) throws IOException;

    Playlist deletePlaylist(int id);

    Playlist createPlaylist(String title, int totalPlaytime, int userID, Set<Track> tracks, Set<Genre> genres, boolean isPublic);

    void generatePlaylist(String startPoint, String endPoint, List<GenrePercentage> allGenres,
                          User creator, String title, boolean isPublic, boolean repeatArtist, boolean useTopRanked);

    String formatTotalPlaytime(int totalPlaytime);

    String getNumberOfTracks(Playlist playlist);

    List<PlaylistMinDto> filter(String criteria, String... value);

    List<PlaylistMinDto> sort(String orderBy);

    List<PlaylistMinDto> searchPlaylists(String keyword);

    Playlist getPrivatePlaylistByName(String username, int playlistID);

    Playlist getPrivatePlaylistByName(String playlistName);
}


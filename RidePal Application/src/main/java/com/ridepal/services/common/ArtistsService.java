package com.ridepal.services.common;

import com.ridepal.models.Artist;
import java.util.List;

public interface ArtistsService {
    List<Artist> getAllArtists();

    List<Artist> getArtistByName(String name);

    Artist getArtistByID(int id);

    Artist getArtistByDeezerID(int deezerId);

    Artist createArtist(String name, int deezerId, String tracklist, int genreId, String picture);
}

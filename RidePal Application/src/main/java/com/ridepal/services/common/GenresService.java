package com.ridepal.services.common;

import com.ridepal.models.Genre;
import java.util.List;

public interface GenresService {
    List<Genre> getAllGenres();

    List<Genre> getGenreByName(String name);

    Genre getGenreById(int id);

    Genre createGenre(String name, int deezerId);

    Genre getGenreByDeezerID(int deezerID);

    Genre getGenreByExactName(String name);

    List<Genre> getGenreByNameForSearchMethod(String name);
}

package com.ridepal.services.common;

import com.ridepal.models.Gender;
import java.util.List;

public interface GenderService {
    List<Gender> getAllGender();

    Gender getGenderByName(String name);

    Gender getGenderByID(int id);
}

package com.ridepal.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.GenresRepository;
import com.ridepal.exceptions.DuplicateEntityException;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.GenresService;
import org.springframework.stereotype.Service;
import static com.ridepal.utils.Constants.*;
import com.ridepal.models.Genre;
import java.util.List;

@Service
public class GenresServiceImpl implements GenresService {
    private GenresRepository genresRepository;

    @Autowired
    public GenresServiceImpl(GenresRepository genresRepository) {
        this.genresRepository = genresRepository;
    }

    @Override
    public List<Genre> getAllGenres() {
        return genresRepository.getAllGenres();
    }

    @Override
    public Genre getGenreById(int id) {
        Genre genre = genresRepository.getGenreById(id);
        if (genre == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENRE_ID_MESSAGE, id));
        }
        return genre;
    }

    @Override
    public List<Genre> getGenreByName(String name) {
        List<Genre> genres = genresRepository.getGenreByName(name);
        if (genres.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENRE_MESSAGE, name));
        }
        return genresRepository.getGenreByName(name);
    }

    public List<Genre> getGenreByNameForSearchMethod(String name) {
        return genresRepository.getGenreByName(name);
    }

    @Override
    public Genre getGenreByExactName(String name) {
        Genre genre = genresRepository.getGenreByExactName(name);
        if (genre == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENRE_MESSAGE, name));
        }
        return genre;
    }

    @Override
    public Genre createGenre(String name, int deezerId) {
        List<Genre> genres = genresRepository.getGenreByName(name);
        if (genres.size() != 0) {
            throw new DuplicateEntityException(
                    String.format(ALREADY_EXISTING_GENRE_MESSAGE, name)); // TODO cant make test
        }
        return genresRepository.createGenre(name, deezerId);
    }

    public Genre getGenreByDeezerID(int deezerID) {
        Genre genre = genresRepository.getGenreByDeezerID(deezerID);
        if (genre == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENRE_ID_MESSAGE, deezerID)
            );
        }
        return genre;
    }
}

package com.ridepal.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.TracksRepository;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.GenresService;
import com.ridepal.services.common.TracksService;
import org.springframework.stereotype.Service;
import static com.ridepal.utils.Constants.*;
import com.ridepal.models.Genre;
import com.ridepal.models.Track;
import java.util.List;

@Service
public class TracksServiceImpl implements TracksService {
    private TracksRepository tracksRepository;
    private GenresService genresService;

    @Autowired
    public TracksServiceImpl(TracksRepository tracksRepository,
                             GenresService genresService) {
        this.tracksRepository = tracksRepository;
        this.genresService = genresService;
    }

    @Override
    public List<Track> getAllTracks() {
        return tracksRepository.getAllTracks();
    }

    @Override
    public Track getTrackById(int id) {
        Track track = tracksRepository.getTrackById(id);
        if (track == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_TRACK_ID_MESSAGE, id));
        }
        return track;
    }

    @Override
    public List<Track> getTrackByName(String name) {
        List<Track> tracks = tracksRepository.getTrackByName(name);
        if (tracks.size() == 0) {
            throw new EntityNotFoundException(String.format(NOT_FOUND_TRACK_MESSAGE, name));
        }
        return tracks;
    }

    public List<Track> getTracksByGenre(int genreID) {
        Genre genre = genresService.getGenreById(genreID);
        if (genre == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_GENRE_ID_MESSAGE, genreID));
        }

        List<Track> tracks = tracksRepository.getTracksByGenre(genre.getDeezerId());
        if (tracks.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_TRACKS_BY_GENRE, genre.getName())
            );
        }
        return tracks;
    }

    @Override
    public Track createTrack(String title, String link,
                             int duration, int deezerId,
                             int artistId, int albumId,
                             int rank, String previewURL) {
        return tracksRepository.createTrack(title, link, duration, deezerId, artistId, albumId, rank, previewURL);
    }

    @Override
    public String formatTotalPlaytime(int totalPlaytime) {
        int minutes = (totalPlaytime % 3600) / 60;
        int seconds = totalPlaytime % 60;

        return String.format("%02d:%02d", minutes, seconds);
    }
}

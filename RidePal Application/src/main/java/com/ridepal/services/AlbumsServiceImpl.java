package com.ridepal.services;

import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.repositories.common.AlbumsRepository;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.services.common.AlbumsService;
import org.springframework.stereotype.Service;
import static com.ridepal.utils.Constants.*;
import com.ridepal.models.Album;
import java.util.List;

@Service
public class AlbumsServiceImpl implements AlbumsService {
    private AlbumsRepository albumsRepository;

    @Autowired
    public AlbumsServiceImpl(AlbumsRepository albumsRepository) {
        this.albumsRepository = albumsRepository;
    }

    @Override
    public List<Album> getAllAlbums() {
        return albumsRepository.getAllAlbums();
    }

    @Override
    public List<Album> getAlbumByName(String name) {
        List<Album> albums = albumsRepository.getAlbumByName(name);
        if (albums.size() == 0) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ALBUM_BY_NAME_MESSAGE, name));
        }
        return albums;
    }

    @Override
    public Album getAlbumByID(int id) {
        Album album = albumsRepository.getAlbumByID(id);
        if (album == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ALBUM_BY_ID_MESSAGE, id));
        }
        return album;
    }

    @Override
    public Album getAlbumByDeezerID(int deezerId) {
        Album album = albumsRepository.getAlbumByDeezerID(deezerId);
        if (album == null){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ALBUM_BY_ID_MESSAGE, deezerId));
        }
        return album;
    }

    @Override
    public Album createAlbum(String title, int deezerId, String tracklist, int genreId, int artistId, String picture) {
        return albumsRepository.createAlbum(title, deezerId, tracklist, genreId, artistId, picture);
    }
}


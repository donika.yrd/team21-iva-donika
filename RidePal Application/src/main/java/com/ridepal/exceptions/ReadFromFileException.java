package com.ridepal.exceptions;

public class ReadFromFileException extends RuntimeException {
    public ReadFromFileException(String message){super(message);}
}

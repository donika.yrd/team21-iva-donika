package com.ridepal.bing_maps_connection;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.ridepal.exceptions.BingMapsConnectionException;
import com.ridepal.exceptions.JSONConverterException;
import com.ridepal.exceptions.ReadFromFileException;
import org.springframework.stereotype.Component;

import java.net.HttpURLConnection;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.net.URL;

import static com.ridepal.utils.Constants.*;

@Component
public class BingMapsConnector {
    public BigInteger bingMapsConnection(RouteRequest routeRequest) {
        try {

            String url=createURLForBingMaps(routeRequest);
            URL requestURL = new URL(url);
            HttpURLConnection request = (HttpURLConnection) requestURL.openConnection();
            request.connect();

            return getDurationFromJSON(request);
        } catch (JSONConverterException j) {
            throw new BingMapsConnectionException(FAILED_BING_MAPS_CONNECTION_MESSAGE);
        } catch (IOException io) {
            throw new ReadFromFileException(INVALID_KEY_VALUE);
        }
    }

    private BigInteger getDurationFromJSON(HttpURLConnection request) {
        try {
            JsonParser jsonParser = new JsonParser();
            JsonElement root = jsonParser.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject response = root.getAsJsonObject();
            JsonElement resourceSets = response.get(RESOURCES_SET_JSON_MEMBER);
            JsonElement resources = resourceSets.getAsJsonArray().get(0);
            JsonElement resourcesElement = resources.getAsJsonObject().get(RESOURCE_JSON_ELEMENT).getAsJsonArray().get(0);
            return resourcesElement.getAsJsonObject().get(TRAVEL_DURATION_JSON_ELEMENT).getAsBigInteger();
        } catch (IOException io) {
            throw new JSONConverterException();
        }
    }

    private String createURLForBingMaps(RouteRequest routeRequest) throws IOException {
        String url = URL_BASE_FOR_BING_MAPS;
        url += FIRST_WAY_POINT_URL + routeRequest.getStart_locality().replace(" ", SPACE_FOR_CITIES_URL);
        url += SECOND_WAY_POINT_URL + routeRequest.getEnd_locality().replace(" ", SPACE_FOR_CITIES_URL);
        url += JSON_OUTPUT_URL;
        url += KEY_URL + readKeyStringFile(PATH_FILE_TO_KEY);

        return url;
    }

    private String readKeyStringFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }

}
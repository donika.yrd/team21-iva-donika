package com.ridepal.bing_maps_connection;

public class RouteRequest {
    private String start_locality;
    private String end_locality;

    public RouteRequest() {}

    public RouteRequest(String start_locality, String end_locality) {
        setStart_locality(start_locality);
        setEnd_locality(end_locality);
    }

    public String getStart_locality() {
        return start_locality;
    }

    public void setStart_locality(String start_locality) {
        this.start_locality = start_locality;
    }

    public String getEnd_locality() {
        return end_locality;
    }

    public void setEnd_locality(String end_locality) {
        this.end_locality = end_locality;
    }
}

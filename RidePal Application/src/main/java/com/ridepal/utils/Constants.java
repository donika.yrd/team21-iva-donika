package com.ridepal.utils;

public class Constants {

    //GenreExceptions
    public static final String NOT_FOUND_GENRE_ID_MESSAGE = "Genre with id %d does not exist.";
    public static final String NOT_FOUND_GENRE_MESSAGE = "Genre: %s does not exist.";
    public static final String ALREADY_EXISTING_GENRE_MESSAGE = "Genre '%s' already exist.";

    //TrackExceptions
    public static final String NOT_FOUND_TRACK_ID_MESSAGE = "Track with id %d does not exist.";
    public static final String NOT_FOUND_TRACK_MESSAGE = "Track: %s does not exist.";

    //PlaylistExceptions
    public static final int MIN_PLAYLIST_NAME_LENGTH = 2;
    public static final int MAX_PLAYLIST_NAME_LENGTH = 50;
    public static final String PLAYLIST_NAME_ERROR_MESSAGE =
            "Playlist's name length should be between {min} and {max} characters long.";

    public static final String NOT_FOUND_PLAYLIST_ID_MESSAGE = "Playlist with id %d does not exist.";
    public static final String NOT_FOUND_PLAYLIST_MESSAGE = "Playlist: %s does not exist.";

    //GenderExceptions
    public static final String NOT_FOUND_GENDER_MESSAGE = "Gender: %s does not exist.";
    public static final String NOT_FOUND_GENDER_BY_ID_MESSAGE = "Gender with id: %d does not exist.";
    public static final String NOT_FOUND_TRACKS_BY_GENRE = "No tracks found under %s genre.";
    public static final String GENDER_NOT_APPLICABLE = "N/A";


    //ArtistExceptions
    public static final String NOT_FOUND_ARTIST_BY_NAME_MESSAGE = "Artist '%s' does not exist.";
    public static final String NOT_FOUND_ARTIST_BY_ID_MESSAGE = "Artist with id: %d does not exist.";
    public static final String ALREADY_EXISTING_ARTIST_MESSAGE = "Artist with name '%s' already exist.";

    //AlbumExceptions
    public static final String NOT_FOUND_ALBUM_BY_NAME_MESSAGE = "Album '%s' does not exist.";
    public static final String NOT_FOUND_ALBUM_BY_ID_MESSAGE = "Album with id: %d does not exist.";
    public static final String ALREADY_EXISTING_ALBUM_MESSAGE = "Album with name '%s' already exist.";

    //UserExceptions
    public static final String NOT_FOUND_USER_BY_ID_MESSAGE = "User with id '%s' does not exist.";
    public static final String NOT_FOUND_USER_BY_USERNAME_MESSAGE = "User with username '%s' does not exist.";
    public static final String ALREADY_EXISTING_USERNAME_MESSAGE = "User with username '%s' already exist.";
    public static final String ALREADY_EXISTING_EMAIL_MESSAGE = "User with e-mail '%s' already exist.";
    public static final String PASSWORDS_DO_NOT_MATCH_MESSAGE = "Passwords do not match. Please try again.";

    //SortingExceptions
    public static final String INVALID_SORTING_CRITERIA = "Invalid sorting criteria - '%s'.";

    //UserDTO validation messages
    public static final int MINIMUM_SIZE_FOR_NAME_FIELDS = 1;
    public static final int MINIMUM_SIZE_FOR_USERNAME = 4;
    public static final int MINIMUM_SIZE_FOR_PASSWORD = 6;
    public static final int MINIMUM_SIZE_FOR_EMAIL = 8;

    public static final int MAXIMUM_SIZE_FOR_NAME_FIELDS = 30;
    public static final int MAXIMUM_SIZE_FOR_USERNAME = 50;
    public static final int MAXIMUM_SIZE_FOR_PASSWORD = 68;
    public static final int MAXIMUM_SIZE_FOR_EMAIL = 50;

    public static final String REQUIRED_FIRST_NAME_MESSAGE = "First name is required.";
    public static final String REQUIRED_LAST_NAME_MESSAGE = "Last name is required.";
    public static final String REQUIRED_USERNAME_MESSAGE = "Username is required.";
    public static final String REQUIRED_PASSWORD_MESSAGE = "Password is required.";
    public static final String REQUIRED_PASSWORD_CONFIRM_MESSAGE = "Password confirmation is required.";
    public static final String REQUIRED_EMAIL_MESSAGE = "E-mail is required.";

    public static final String UNSUITABLE_FIRST_NAME_MASSAGE = "First name should be between {min} and {max} symbols.";
    public static final String UNSUITABLE_LAST_NAME_MASSAGE = "Last name should be between {min} and {max} symbols.";
    public static final String UNSUITABLE_USERNAME_MASSAGE = "Username should be between {min} and {max} symbols.";
    public static final String UNSUITABLE_PASSWORD_MASSAGE = "Password should be between {min} and {max} symbols.";
    public static final String UNSUITABLE_EMAIL_MESSAGE = "E-mail should be between {min} and {max} symbols.";

    //Deezer
    public static final int MAX_NUMBER_OF_TRACKS_PER_ALBUM = 10;
    public static final int MAX_NUMBER_OF_ALBUMS_PER_ARTIST = 10;

    //Bing Maps Connection messages
    public static final String FIRST_WAY_POINT_URL = "?wp.0=";
    public static final String SECOND_WAY_POINT_URL = "&wp.1=";
    public static final String JSON_OUTPUT_URL = "&output=json";
    public static final String KEY_URL = "&key=";
    public static final String SPACE_FOR_CITIES_URL = "%20";
    public static final String RESOURCES_SET_JSON_MEMBER = "resourceSets";
    public static final String RESOURCE_JSON_ELEMENT = "resources";
    public static final String TRAVEL_DURATION_JSON_ELEMENT = "travelDuration";
    public static final String PATH_FILE_TO_KEY = "src\\Bing_Maps_key.txt";
    public static final String URL_BASE_FOR_BING_MAPS = "http://dev.virtualearth.net/REST/V1/Routes/Driving";
    public static final String FAILED_BING_MAPS_CONNECTION_MESSAGE = "Connection with Bing Maps failed. " + "\n" + "Please try" +
            " again and check your inputs for cities.";
    public static final String INVALID_KEY_VALUE = "Access to Bing Maps denied.Invalid key value.";

    //Empty Data
    public static final String EMPTY_LIST_OF_GENRES="Please select genre(s) and valid percentages in order to generate playlist.";
}

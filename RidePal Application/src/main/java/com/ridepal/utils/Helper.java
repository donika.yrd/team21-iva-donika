package com.ridepal.utils;

import com.ridepal.bing_maps_connection.BingMapsConnector;
import com.ridepal.exceptions.EntityNotFoundException;
import com.ridepal.bing_maps_connection.RouteRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import com.ridepal.models.dtoes.PlaylistMinDto;
import static com.ridepal.utils.Constants.*;
import com.ridepal.models.*;


import org.json.JSONArray;
import java.util.*;

public class Helper {
    public static Artist setArtist(String name, int deezerId, String tracklist, int genreId, String picture) {
        Artist newArtist = new Artist();
        newArtist.setName(name);
        newArtist.setDeezerId(deezerId);
        newArtist.setTracklist_URL(tracklist);
        newArtist.setGenreID(genreId);
        newArtist.setPicture(picture);
        return newArtist;
    }

    public static Album setAlbum(String title, int deezerId, String tracklist, int genreId, String picture) {
        Album newAlbum = new Album();
        newAlbum.setName(title);
        newAlbum.setDeezerId(deezerId);
        newAlbum.setTracklist_URL(tracklist);
        newAlbum.setGenreID(genreId);
        newAlbum.setPicture(picture);
        return newAlbum;
    }

    public static Track setTrack(String title, String link, int duration, int deezerId, int rank, String previewURL) {
        Track newTrack = new Track();
        newTrack.setTitle(title);
        newTrack.setLink(link);
        newTrack.setDuration(duration);
        newTrack.setDeezerId(deezerId);
        newTrack.setRank(rank);
        newTrack.setPreviewURL(previewURL);
        return newTrack;
    }


    public static int checkNumberOfTracks(JSONArray array) {
        int numberOfTracks = array.length();
        if (numberOfTracks > MAX_NUMBER_OF_TRACKS_PER_ALBUM) {
            numberOfTracks = MAX_NUMBER_OF_TRACKS_PER_ALBUM;
        }
        return numberOfTracks;
    }

    public static int checkNumberOfAlbums(JSONArray array) {
        int numberOfAlbums = array.length();
        if (numberOfAlbums > MAX_NUMBER_OF_ALBUMS_PER_ARTIST) {
            numberOfAlbums = MAX_NUMBER_OF_ALBUMS_PER_ARTIST;
        }
        return numberOfAlbums;
    }

    public static boolean checkIfUserExists(List<User> userWithExactUsername) {
        return userWithExactUsername.size() != 0;
    }

    public static void ifNotExist(String parameter, String value, boolean exists) {
        if (!exists) {
            String errorMsg;
            switch (parameter) {
                case "username":
                    errorMsg = NOT_FOUND_USER_BY_USERNAME_MESSAGE;
                    break;
                case "id":
                    errorMsg = NOT_FOUND_USER_BY_ID_MESSAGE;
                    break;
                default:
                    errorMsg = "";
            }
            throw new EntityNotFoundException(
                    String.format(errorMsg, value));
        }
    }

    public static String getErrorMessage(BindingResult bindingResult, String error) {
        if (bindingResult.getFieldError() != null) {
            error = bindingResult.getFieldError().getDefaultMessage();
        }
        if (bindingResult.getGlobalError() != null) {
            error = bindingResult.getGlobalError().getDefaultMessage()
                    .replace(bindingResult.getGlobalError().getDefaultMessage(), PASSWORDS_DO_NOT_MATCH_MESSAGE);
        }
        return error;
    }

    public static void updateParameters(User userToUpdate, User update) {
        if (update.getPassword() != null) {
            userToUpdate.setPassword(update.getPassword());
        }
        if (update.getFirstName() != null) {
            userToUpdate.setFirstName(update.getFirstName());
        }
        if (update.getLastName() != null) {
            userToUpdate.setLastName(update.getLastName());
        }
        if (update.getEmail() != null) {
            userToUpdate.setEmail(update.getEmail());
        }
        if (update.getGender() != null) {
            userToUpdate.setGender(update.getGender());
        }
        if (update.getPicture() != null) {
            userToUpdate.setPicture(update.getPicture());
        }
        if (update.getPlaylists() != null) {
            userToUpdate.setPlaylists(update.getPlaylists());
        }
        if (update.getBirthDate() != null) {
            userToUpdate.setBirthDate(update.getBirthDate());
        }
    }

    public static int calculatePlaylistAverageRank(Playlist generatedPlaylist) {
        int tracksRankSum = 0;
        int trackNumber = generatedPlaylist.getTracks().size();

        for (Track track : generatedPlaylist.getTracks()) {
            tracksRankSum += track.getRank();
        }
        return tracksRankSum / trackNumber;
    }

    public static List<PlaylistMinDto> fillListOfPlaylistsIntoDtos(List<Playlist> playlists) {
        List<PlaylistMinDto> playlistDtos = new ArrayList<>();
        Set<String> genres = new HashSet<>();
        for (Playlist playlist : playlists) {
            for (Genre genre : playlist.getGenresTags()) {
                genres.add(genre.getName());
            }
            playlistDtos.add(
                    new PlaylistMinDto(
                            playlist.getId(),
                            playlist.getRank(),
                            playlist.getTitle(),
                            playlist.getPicture(),
                            playlist.getPlaytime(),
                            playlist.getCreatorName(),
                            genres));
        }
        return playlistDtos;
    }

    public static String getLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public static int getDurationFromBingMaps(String startPoint, String endPoint){
        RouteRequest routeRequest = new RouteRequest(startPoint, endPoint);
        BingMapsConnector bingMapsConnector = new BingMapsConnector();
        return bingMapsConnector.bingMapsConnection(routeRequest).intValue();
    }

}

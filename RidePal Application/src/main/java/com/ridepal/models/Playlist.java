package com.ridepal.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Playlists")
public class Playlist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "Enabled")
    private boolean enabled;

    @Column(name = "isPublic")
    private boolean isPublic;

    @Column(name = "Title")
    private String title;

    @Column(name = "Total_Playtime")
    private int totalPlaytime;

    @Column(name = "CreatorID")
    private int creatorID;

    @Column(name = "Picture")
    private String picture;

    @Column(name = "Rank")
    private int rank;

    @Transient
    private String playtime;

    @Transient
    private String numberOfTracks;

    @Transient
    private String creatorName;

    @ManyToMany
    @JoinTable(name = "Playlists_Genres",
            joinColumns = @JoinColumn(name = "PlaylistID"),
            inverseJoinColumns = @JoinColumn(name = "GenreID"))
    private Set<Genre> genresTags;

    @ManyToMany
    @JoinTable(name = "playlists_tracks",
            joinColumns = @JoinColumn(name = "PlaylistID"),
            inverseJoinColumns = @JoinColumn(name = "TrackID"))
    private Set<Track> tracks;

    public Playlist() {
        enabled = true;
        genresTags = new HashSet<>();
    }

    public Set<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTotalPlaytime() {
        return totalPlaytime;
    }

    public void setTotalPlaytime(int totalPlaytime) {
        this.totalPlaytime = totalPlaytime;
    }

    public Set<Genre> getGenresTags() {
        return genresTags;
    }

    public void setGenresTags(Set<Genre> genresTags) {
        this.genresTags = genresTags;
    }

    public void addGenreTag(Genre genre) {
        genresTags.add(genre);
    }

    public int getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(int creatorID) {
        this.creatorID = creatorID;
    }

    public String getPlaytime() {
        return playtime;
    }

    public void setPlaytime(String playtime) {
        this.playtime = playtime;
    }

    public String getNumberOfTracks() {
        return numberOfTracks;
    }

    public void setNumberOfTracks(String numberOfTracks) {
        this.numberOfTracks = numberOfTracks;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}

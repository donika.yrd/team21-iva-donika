package com.ridepal.models;

public class GenrePercentage {
    private Integer percentage;
    private Genre genre;

    public GenrePercentage(){}

    public GenrePercentage(Genre genre, Integer percentage){
        setGenre(genre);
        setPercentage(percentage);
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}

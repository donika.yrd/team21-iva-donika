package com.ridepal.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Fetch;
import java.io.Serializable;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "artists")
public class Artist implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int ID;

    @Column(name = "Name")
    private String name;

    @Column(name = "Tracklist_URL")
    private String tracklist_URL;

    @JsonManagedReference(value = "aristRef")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "artist", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private Set<Track> tracks;

    @Column(name = "DeezerID")
    private int deezerId;

    @Column(name = "GenreID")
    private int genreID;

    @JsonManagedReference(value = "artistRef")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "artist", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private Set<Album> albums;

    @Lob
    @Column(name = "Picture")
    private String picture;

    public Artist() {
        tracks = new HashSet<>();
        albums = new HashSet<>();
    }

    public Artist(String name, String tracklist_URL) {
        this.name = name;
        this.tracklist_URL = tracklist_URL;
        tracks = new HashSet<>();
        albums = new HashSet<>();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTracklist_URL() {
        return tracklist_URL;
    }

    public void setTracklist_URL(String tracklist_URL) {
        this.tracklist_URL = tracklist_URL;
    }

    public Set<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public int getDeezerId() {
        return deezerId;
    }

    public void setDeezerId(int deezerId) {
        this.deezerId = deezerId;
    }

    public int getGenreID() {
        return genreID;
    }

    public void setGenreID(int genreID) {
        this.genreID = genreID;
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

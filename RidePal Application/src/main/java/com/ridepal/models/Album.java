package com.ridepal.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Fetch;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name ="albums")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int ID;

    @Column(name = "Name")
    private String name;

    @Column(name = "Tracklist_URL")
    private String tracklist_URL;

    @JsonManagedReference(value = "albumRef")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "album", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private Set<Track> tracks;

    @Column(name = "DeezerID")
    private int deezerId;

    @Column(name = "GenreID")
    private int genreID;

    @JsonBackReference(value = "artistRef")
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ArtistID")
    private Artist artist;

    @Transient
    private Genre genre;

    @Lob
    @Column(name = "Picture")
    private String picture;

    public Album(){
        tracks = new HashSet<>();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTracklist_URL() {
        return tracklist_URL;
    }

    public void setTracklist_URL(String tracklist_URL) {
        this.tracklist_URL = tracklist_URL;
    }

    public Set<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public int getDeezerId() {
        return deezerId;
    }

    public void setDeezerId(int deezerId) {
        this.deezerId = deezerId;
    }

    public int getGenreID() {
        return genreID;
    }

    public void setGenreID(int genreID) {
        this.genreID = genreID;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

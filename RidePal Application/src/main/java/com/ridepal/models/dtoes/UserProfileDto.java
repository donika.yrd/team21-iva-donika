package com.ridepal.models.dtoes;

import org.hibernate.validator.constraints.ScriptAssert;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.ridepal.models.Playlist;
import java.sql.Date;
import java.util.Set;

import static com.ridepal.utils.Constants.*;

@ScriptAssert(lang = "javascript", alias = "_",
        script = "_.passwordConfirmation != null && _.passwordConfirmation.equals(_.password)")
public class UserProfileDto {
    @NotBlank(message = REQUIRED_FIRST_NAME_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_NAME_FIELDS, max = MAXIMUM_SIZE_FOR_NAME_FIELDS, message = UNSUITABLE_FIRST_NAME_MASSAGE)
    private String firstName;

    @NotBlank(message = REQUIRED_LAST_NAME_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_NAME_FIELDS, max = MAXIMUM_SIZE_FOR_NAME_FIELDS, message = UNSUITABLE_LAST_NAME_MASSAGE)
    private String lastName;

    @NotNull
    @NotBlank(message = REQUIRED_USERNAME_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_USERNAME, max = MAXIMUM_SIZE_FOR_USERNAME, message = UNSUITABLE_USERNAME_MASSAGE)
    private String username;

    @NotNull
    @NotBlank(message = REQUIRED_PASSWORD_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_PASSWORD, max = MAXIMUM_SIZE_FOR_PASSWORD, message = UNSUITABLE_PASSWORD_MASSAGE)
    private String password;

    @NotNull
    @NotBlank(message = REQUIRED_PASSWORD_CONFIRM_MESSAGE)
    private String passwordConfirmation;

    @NotNull
    @NotBlank(message = REQUIRED_EMAIL_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_EMAIL, max = MAXIMUM_SIZE_FOR_EMAIL, message = UNSUITABLE_EMAIL_MESSAGE)
    private String email;

    private Set<Playlist> playlistSet;

    private int genderID;

    private Date birthDate;

    public UserProfileDto() {
    }

    private String picture;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGenderID() {
        return genderID;
    }

    public void setGenderID(int genderID) {
        this.genderID = genderID;
    }

    public Set<Playlist> getPlaylistSet() {
        return playlistSet;
    }

    public void setPlaylistSet(Set<Playlist> playlistSet) {
        this.playlistSet = playlistSet;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}

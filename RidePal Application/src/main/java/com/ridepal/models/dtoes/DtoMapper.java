package com.ridepal.models.dtoes;

import com.ridepal.exceptions.DataNotFoundException;
import com.ridepal.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ridepal.services.common.GenderService;
import com.ridepal.services.common.GenresService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.ridepal.utils.Constants.EMPTY_LIST_OF_GENRES;

@Component
public class DtoMapper {
    private GenresService genresService;
    private GenderService genderService;

    @Autowired
    public DtoMapper(GenresService genresService, GenderService genderService) {
        this.genresService = genresService;
        this.genderService = genderService;
    }

    public Playlist fromPlaylistMinDto(PlaylistMinDto playlistMinDto) {
        Playlist playlist = new Playlist();

        playlist.setCreatorName(playlistMinDto.getCreatorName());
        playlist.setPlaytime(playlistMinDto.getDuration());
        playlist.setPicture(playlistMinDto.getPicture());
        playlist.setPublic(playlistMinDto.isPublic());
        playlist.setTitle(playlistMinDto.getTitle());
        playlist.setRank(playlistMinDto.getRank());
        playlist.setId(playlistMinDto.getId());

        for (String genre : playlistMinDto.getGenresTags()) {
            Genre g = genresService.getGenreByName(genre).get(0);
            playlist.addGenreTag(g);
        }
        return playlist;
    }

    public User fromUseRegDto(UserRegDto userRegDTO) {
        User user = new User();
        user.setUsername(userRegDTO.getUsername());
        user.setPassword(userRegDTO.getPassword());

        return user;
    }

    public User fromUserProfileDto(UserProfileDto userProfileDto){
        User user = new User();
        user.setPlaylists(userProfileDto.getPlaylistSet());
        user.setFirstName(userProfileDto.getFirstName());
        user.setBirthDate(userProfileDto.getBirthDate());
        user.setUsername(userProfileDto.getUsername());
        user.setPassword(userProfileDto.getPassword());
        user.setLastName(userProfileDto.getLastName());
        user.setPicture(userProfileDto.getPicture());
        user.setEmail(userProfileDto.getEmail());

        Gender gender = genderService.getGenderByID(userProfileDto.getGenderID());
        user.setGender(gender);

        return user;
    }

    public List<GenrePercentage> fromGenrePercentageDTO(GeneratePlaylistAlgDTO dto) {
        List<GenrePercentage> realGenrePercentage = new ArrayList<>();

        List<GenrePercentageDTO> genrePercentageDTOS = dto.getPickGenres();
        for (GenrePercentageDTO gpd : genrePercentageDTOS) {
            if (gpd.getGenreName() != null && gpd.getPercentage() != null && gpd.getPercentage() > 0) {
                Genre genre = genresService.getGenreByExactName(gpd.getGenreName());
                GenrePercentage newGenrePercentage = new GenrePercentage(genre, gpd.getPercentage());
                realGenrePercentage.add(newGenrePercentage);
            }
        }
        return checkCorrectnessOfGenres(realGenrePercentage);
    }

    private List<GenrePercentage> checkCorrectnessOfGenres(List<GenrePercentage> dto) {
        Integer percentageSum = 0;

        if (dto.size() == 0) {
            throw new DataNotFoundException(EMPTY_LIST_OF_GENRES);
        }

        for (GenrePercentage gp : dto) {
            percentageSum += gp.getPercentage();
        }

        if (percentageSum < 100) {
            Integer deficiency = (100 - percentageSum) / dto.size();
            for (GenrePercentage gp : dto) {
                gp.setPercentage(gp.getPercentage() + deficiency);
            }
        }
        if (percentageSum > 100) {
            Integer redundancy = (percentageSum - 100) / dto.size();
            for (GenrePercentage gp : dto) {
                gp.setPercentage(gp.getPercentage() - redundancy);
            }
        }
        return dto;
    }
}

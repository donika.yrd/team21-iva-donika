package com.ridepal.models.dtoes;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

import static com.ridepal.utils.Constants.*;

public class PlaylistMinDto {
    @PositiveOrZero
    private int id;

    @Size(min = MIN_PLAYLIST_NAME_LENGTH, max = MAX_PLAYLIST_NAME_LENGTH, message = PLAYLIST_NAME_ERROR_MESSAGE)
    private String title;

    private String picture;

    private boolean isPublic;

    private Set<String> genresTags;

    private String creatorName;

    private String duration;

    private int rank;

    public PlaylistMinDto() {
        genresTags = new HashSet<>();
    }

    public PlaylistMinDto(int id,
                          int rank,
                          String title,
                          String picture,
                          String duration,
                          String creatorName,
                          Set<String> genresTags) {
        this.creatorName = creatorName;
        this.genresTags = genresTags;
        this.duration = duration;
        this.picture = picture;
        this.title = title;
        this.rank = rank;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<String> getGenresTags() {
        return genresTags;
    }

    public void setGenresTags(Set<String> genresTags) {
        this.genresTags = genresTags;
    }

    public void addGenreTag(String genre) {
        genresTags.add(genre);
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}

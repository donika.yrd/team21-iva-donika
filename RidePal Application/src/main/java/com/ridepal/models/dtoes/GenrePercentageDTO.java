package com.ridepal.models.dtoes;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class GenrePercentageDTO {
    @NotNull
    @NotBlank
    private String genreName;

    @NotNull
    @Positive
    private Integer percentage;

    public GenrePercentageDTO(){}

    public GenrePercentageDTO(String genreName,Integer percentage){
        this.genreName = genreName;
        this.percentage = percentage;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
